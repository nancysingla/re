$(document).ready(function () {
	var interval = null,
		rno, elem, pos, id, position, top, position2, top2, score = 0;
	//:::::::::::::::::function to send random obstacles:::::::::::::::::
	interval = setInterval(function () {
		rno = (Math.floor((Math.random() * 350)));
		$('#container').append(`<div class="animate" style='top:${rno}px;left:350px' id=${rno}><img src= './Assests/image/bullet.png' class='bullet' alt="bullet icon" height="50px" width="50px"></div>`);
		elem = $(`#${rno}`);
		pos = 350;
		id = setInterval(frame, 0);

		function frame() {
			if (pos == 0) {
				clearInterval(id);
				$(elem[0]).remove();
			} else {
				pos--;
				elem[0].style.left = pos + "px";
				if (pos == 0) {
					score = score + 100;
					$('#score').html(score);
				}
				if (pos <= 50) {
					position = $('#animate').position();
					top = position.top;
					position2 = $(elem[0]).position();
					top2 = position2.top;
					//checking if obstacle collapsed with body
					if (Math.abs(top2 - top) <= 30) {
						//:::::::::::::::GAME OVER::::::::::::::::
						pos = 0;
						clearInterval(interval);
						$('#tudo').show();
						$('#animate').hide();
						$('#finalscore').html(score);
					}

				}

			}
		}
	}, 2000);
	//:::::::::::::::::MOVING BODY UP & DOWN:::::::::::::::::
	$(document).keydown(function (e) {
		position = $('#animate').position();
		posit = position.top;
		switch (e.which) {
			case 38:
				if(posit>=20)
				{
					$('#animate').stop().animate({
						top: '-=20px'
					}, 300, "linear"); //up arrow key
					
				}
				break;
				
			case 40:
				if(posit<=337)
				{
					$('#animate').stop().animate({
						top: '+=20px'
					}, 300, "linear"); //bottom arrow key
				}
				break;
		}
	});
});